package com.abd.androidnotifier;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LinearLayout linearLayout = findViewById(R.id.background_layout);
        TextView textView = findViewById(R.id.text_view);
        int backgroundColor = Color.BLACK;
        int textColor = Color.WHITE;

        String message = "ABD Android Notifier";

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String verbose = bundle.getString("verbose");
            String debug = bundle.getString("debug");
            String info = bundle.getString("info");
            String warn = bundle.getString("warn");
            String error = bundle.getString("error");

            if (verbose != null && !verbose.equals("")) {
                backgroundColor = Color.GRAY;
                textColor = Color.WHITE;
                message = verbose;
            } else if (debug != null && !debug.equals("")) {
                backgroundColor = Color.LTGRAY;
                textColor = Color.BLACK;
                message = debug;
            } else if (info != null && !info.equals("")) {
                backgroundColor = Color.BLACK;
                textColor = Color.WHITE;
                message = info;
            } else if (warn != null && !warn.equals("")) {
                backgroundColor = Color.YELLOW;
                textColor = Color.BLACK;
                message = warn;
            } else if (error != null && !error.equals("")) {
                backgroundColor = Color.RED;
                textColor = Color.WHITE;
                message = error;
            }
        }

        linearLayout.setBackgroundColor(backgroundColor);
        textView.setTextColor(textColor);
        textView.setText(message);
    }
}
